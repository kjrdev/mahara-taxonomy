<?php
/**
 *
 * @package    mahara
 * @subpackage module-taxonomy
 * @author     EdICT Training Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

define('INTERNAL', 1);
require('../../init.php');

$smarty = smarty();
$smarty->display('module:taxonomy:index.tpl');
