<?php
/**
 *
 * @package    mahara
 * @subpackage module-taxonomy
 * @author     EdICT Training Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

$string['testcheckboxtitle'] = 'Tick this checkbox?';
$string['testcheckboxdesc'] = 'If you tick this checkbox, it will remain ticked.';

$string['heythere'] = "Hey there!";
$string['taxonomy'] = "Taxonomy";
$string['taxonomymanager'] = "Taxonomy Manager";