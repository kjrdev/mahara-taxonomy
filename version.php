<?php
/**
 *
 * @package    mahara
 * @subpackage module-taxonomy
 * @author     EdICT Training Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$config = new stdClass();
$config->version = 2015031600;
$config->release = '1.0.0';
