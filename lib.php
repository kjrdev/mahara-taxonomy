<?php
/**
 *
 * @package    mahara
 * @subpackage module-taxonomy
 * @author     EdICT Training Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

class PluginModuleTaxonomy extends PluginModule {
    public static function postinst($prevversion) {
        if ($prevversion == 0) {
            insert_record('module_taxonomy_instance', (object) array('field'=>'hello', 'value'=>'How are you?'));
            set_config_plugin('module', 'taxonomy', 'testcheckbox', '1');
            log_info('Config test: ' . get_config_plugin('module', 'taxonomy', 'testcheckbox'));
        }
    }

    public static function get_cron() {
        return array(
            (object) array(
                'callfunction' => 'crontest',
                'minute' => '*',
                'hour' => '*'
            )
        );
    }

    public static function crontest() {
        log_info("Cron called successfully!");
    }

    public static function has_config() {
        return true;
    }

    public static function get_config_options() {
        return array(
            'elements' => array(
                'testcheckbox' => array(
                    'title' => get_string('testcheckboxtitle', 'module.taxonomy'),
                    'description' => get_string('testcheckboxdesc', 'module.taxonomy'),
                    'type' => 'checkbox',
                    'defaultvalue' => get_config_plugin('module', 'taxonomy', 'foo'),
                )
            )
        );
    }

    public static function save_config_options($form, $values) {
        set_config_plugin('module', 'taxonomy', 'foo', (int) $values['testcheckbox']);
    }

    public static function can_be_disabled() {
        return true;
    }
  /**
   * This function returns an array of menu items
   * to be displayed
   *
   * See the function find_menu_children() in lib/web.php
   * for a description of the expected array structure.
   *
   * @return array
   */
  public static function menu_items() {
    return array(
      'configsite/taxonomy' => array(
        'path'   => 'configsite/taxonomy',
        'url' => 'module/taxonomy/index.php',
        'title' => get_string('taxonomy', 'module.taxonomy'),
        'weight' => 100,
      ),
    );
  }

}